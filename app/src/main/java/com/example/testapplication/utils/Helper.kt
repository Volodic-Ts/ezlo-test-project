package com.example.testapplication.utils

import kotlin.random.Random

fun generateHomeId(): Int {
    return Random.nextInt(100000, 1000000)
}