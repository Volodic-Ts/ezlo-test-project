package com.example.testapplication.utils

import com.example.testapplication.R

enum class Platform(val resourceName: String, val resourceId: Int) {
    SERCOMM_G450("Vera Plus", R.mipmap.vera_plus_big),
    SERCOMM_G550("Vera Secure", R.mipmap.vera_secure_big),
    MCV_VERALITE("Vera Edge", R.mipmap.vera_edge_big),
    SERCOMM_NA900("Vera Edge", R.mipmap.vera_edge_big),
    SERCOMM_NA301("Vera Edge", R.mipmap.vera_edge_big),
    SERCOMM_NA930("Vera Edge", R.mipmap.vera_edge_big),
    UNKNOWN("Vera Edge", R.mipmap.vera_edge_big);

    companion object {
        fun fromString(platform: String?): Platform {
            return when (platform) {
                "Sercomm G450" -> SERCOMM_G450
                "Sercomm G550" -> SERCOMM_G550
                "MiCasaVerde VeraLite" -> MCV_VERALITE
                "Sercomm NA900" -> SERCOMM_NA900
                "Sercomm NA301" -> SERCOMM_NA301
                "Sercomm NA930" -> SERCOMM_NA930
                else -> UNKNOWN
            }
        }
    }
}