package com.example.testapplication.ui.view.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.testapplication.R
import com.example.testapplication.data.model.DeviceEntity
import com.example.testapplication.databinding.FragmentDetailsBinding
import com.example.testapplication.ui.common.BaseFragment
import com.example.testapplication.ui.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class DetailsFragment : BaseFragment<FragmentDetailsBinding>(FragmentDetailsBinding::inflate) {

    private val viewModel: MainViewModel by viewModels()
    private lateinit var deviceEntity: DeviceEntity
    private var isEditMode: Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            deviceEntity = DetailsFragmentArgs.fromBundle(it).deviceEntity
            isEditMode = DetailsFragmentArgs.fromBundle(it).isEditMode
        }

        with(binding) {
            homeTitle.text = getString(R.string.home_title, deviceEntity.homeTitle)
            snText.text = getString(R.string.sn, "${deviceEntity.pkDevice}")
            macAddressText.text = getString(R.string.mac_address, deviceEntity.macAddress)
            firmwareText.text = getString(R.string.firmware, deviceEntity.firmware)
            modelText.text = getString(R.string.model, deviceEntity.deviceModel)

            saveBtn.setOnClickListener {
                saveNewHomeTitle(homeTitleEditText.text.toString())
            }
        }

        changeUiState(isEditMode)
    }

    private fun changeUiState(flag: Boolean) {
        if (flag) {
            binding.homeTitle.visibility = View.INVISIBLE
            binding.homeTitleEdit.visibility = View.VISIBLE
            binding.homeTitleEditText.visibility = View.VISIBLE
            binding.saveBtn.visibility = View.VISIBLE
        } else {
            binding.homeTitle.visibility = View.VISIBLE
            binding.homeTitleEdit.visibility = View.INVISIBLE
            binding.homeTitleEditText.visibility = View.INVISIBLE
            binding.saveBtn.visibility = View.INVISIBLE
        }
    }

    private fun saveNewHomeTitle(newHomeTitle: String) {
        lifecycleScope.launch {
            deviceEntity = deviceEntity.copy(homeTitle = newHomeTitle)
            viewModel.updateDevice(deviceEntity)
        }
    }
}