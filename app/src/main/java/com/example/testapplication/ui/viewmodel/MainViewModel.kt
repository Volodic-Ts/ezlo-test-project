package com.example.testapplication.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testapplication.data.model.DeviceEntity
import com.example.testapplication.data.repository.DeviceRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: DeviceRepository
) : ViewModel() {

    private val _devicesStateFlow = MutableStateFlow<List<DeviceEntity>>(emptyList())
    val devicesStateFlow: StateFlow<List<DeviceEntity>> = _devicesStateFlow.asStateFlow()

    init {
        fetchAndLoadDevices()
    }

    fun fetchAndLoadDevices() {
        viewModelScope.launch {
            repository.fetchDevices()
            val devicesFromDb = repository.getAllDevices()
            val sortedDevices = devicesFromDb.sortedBy { it.pkDevice }
            _devicesStateFlow.tryEmit(sortedDevices)
        }
    }

    fun removeSavedDevices() {
        viewModelScope.launch {
            repository.deleteAllDevices()
        }
    }

    fun removeDevice(device: DeviceEntity) {
        viewModelScope.launch {
            repository.deleteDevice(device)
            val updatedDevices = repository.getAllDevices().sortedBy { it.pkDevice }
            _devicesStateFlow.tryEmit(updatedDevices)
        }
    }

    fun updateDevice(device: DeviceEntity) {
        viewModelScope.launch {
            repository.updateDevice(device)
            val updatedDevices = repository.getAllDevices().sortedBy { it.pkDevice }
            _devicesStateFlow.tryEmit(updatedDevices)
        }
    }
}