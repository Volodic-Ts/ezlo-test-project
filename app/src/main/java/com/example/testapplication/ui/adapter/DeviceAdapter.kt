package com.example.testapplication.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.testapplication.R
import com.example.testapplication.data.model.DeviceEntity
import com.example.testapplication.databinding.ItemRawDataBinding
import com.example.testapplication.databinding.ItemRawNoDataBinding
import com.example.testapplication.utils.Platform

private const val EMPTY_LIST_TYPE = 0
private const val NON_EMPTY_LIST_TYPE = 1

class DeviceAdapter(
    private val onItemLongClicked: (DeviceEntity) -> Boolean,
    private val onEditClicked: (DeviceEntity) -> Unit,
    private val onDetailsClicked: (DeviceEntity) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: List<DeviceEntity> = listOf()

    inner class DeviceHolder(private val binding: ItemRawDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            item: DeviceEntity,
            onItemLongClicked: (DeviceEntity) -> Boolean,
            onEditClicked: (DeviceEntity) -> Unit,
            onDetailsClicked: (DeviceEntity) -> Unit
        ) {
            with(binding) {
                homeTitleItem.text = root.context.getString(R.string.home_title, item.homeTitle)
                snTextItem.text = root.context.getString(R.string.sn, "${item.pkDevice}")

                val platformEnum = Platform.fromString(item.deviceModel)
                deviceImageItem.setImageResource(platformEnum.resourceId)

                root.setOnLongClickListener {
                    onItemLongClicked(item)
                    true
                }
                editBtn.setOnClickListener { onEditClicked(item) }
                detailsBtn.setOnClickListener { onDetailsClicked(item) }
            }
        }
    }

    inner class EmptyHolder(binding: ItemRawNoDataBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return if (viewType == EMPTY_LIST_TYPE) {
            val binding = ItemRawNoDataBinding.inflate(inflater, parent, false)
            EmptyHolder(binding)
        } else {
            val binding = ItemRawDataBinding.inflate(inflater, parent, false)
            DeviceHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is DeviceHolder && items.isNotEmpty()) {
            holder.bind(items[position], onItemLongClicked, onEditClicked, onDetailsClicked)
        }
    }

    override fun getItemCount(): Int {
        return if (items.isEmpty()) {
            1
        } else {
            items.size
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (items.isEmpty()) EMPTY_LIST_TYPE else NON_EMPTY_LIST_TYPE
    }

    fun updateItems(newItems: List<DeviceEntity>) {
        val previousItemCount = items.size
        items = newItems
        if (previousItemCount > 0 && items.isEmpty()) {
            notifyDataSetChanged()
        } else {
            notifyDataSetChanged()
        }
    }
}