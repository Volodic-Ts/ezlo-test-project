package com.example.testapplication.ui.view.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testapplication.R
import com.example.testapplication.data.model.DeviceEntity
import com.example.testapplication.databinding.DialogCustomAlertBinding
import com.example.testapplication.databinding.FragmentAvailableDataBinding
import com.example.testapplication.ui.adapter.DeviceAdapter
import com.example.testapplication.ui.common.BaseFragment
import com.example.testapplication.ui.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch

@AndroidEntryPoint
class AvailableDataFragment :
    BaseFragment<FragmentAvailableDataBinding>(FragmentAvailableDataBinding::inflate) {

    private val viewModel: MainViewModel by viewModels()
    private lateinit var adapter: DeviceAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            deviceList.layoutManager = LinearLayoutManager(requireContext())
            adapter = DeviceAdapter(
                onItemLongClicked = { item ->
                    showAlertDialog(item)
                    true
                },
                onDetailsClicked = { item ->
                    Log.e("TAG_DEVICES", "clicked: $item")
                    val action = AvailableDataFragmentDirections.actionNavigationAvailableDataToNavigationDetails(item, false)
                    findNavController().navigate(action)
                },
                onEditClicked = { item ->
                    val action = AvailableDataFragmentDirections.actionNavigationAvailableDataToNavigationDetails(item, true)
                    findNavController().navigate(action)
                })

            deviceList.adapter = adapter

            lifecycleScope.launch {
                viewModel.devicesStateFlow.collectLatest { items ->
                    if (items.isNotEmpty()) {
                        adapter.updateItems(items)
                    }
                }
            }

            resetButton.setOnClickListener {
                viewModel.removeSavedDevices()
                viewModel.fetchAndLoadDevices()
            }
        }

        viewModel.fetchAndLoadDevices()
    }

    private fun showAlertDialog(item: DeviceEntity) {
        val dialogView =
            LayoutInflater.from(requireContext()).inflate(R.layout.dialog_custom_alert, null)
        val binding = DialogCustomAlertBinding.bind(dialogView)

        val dialog = AlertDialog.Builder(requireContext(), R.style.CustomAlertDialogTheme)
            .setView(binding.root)
            .create()

        binding.yesBtn.setOnClickListener {
            viewModel.removeDevice(item)
            lifecycleScope.launch {
                viewModel.devicesStateFlow.collectLatest { items ->
                    if (items.isEmpty()) {
                        adapter.updateItems(emptyList())
                    } else {
                        adapter.updateItems(items)
                    }
                }
            }
            dialog.dismiss()
        }

        binding.noBtn.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }
}