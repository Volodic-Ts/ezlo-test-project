package com.example.testapplication.data.remote.api

import com.example.testapplication.data.remote.response.DeviceListResponse
import retrofit2.http.GET

interface DeviceService {

    @GET("test_android/items.test")
    suspend fun getDevices(): DeviceListResponse
}