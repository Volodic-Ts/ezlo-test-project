package com.example.testapplication.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.testapplication.data.local.dao.DeviceDao
import com.example.testapplication.data.model.DeviceEntity

@Database(entities = [DeviceEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun deviceDao(): DeviceDao
}