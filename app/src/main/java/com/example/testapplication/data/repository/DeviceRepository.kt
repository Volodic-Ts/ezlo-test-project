package com.example.testapplication.data.repository

import android.content.Context
import android.util.Log
import com.example.testapplication.data.local.dao.DeviceDao
import com.example.testapplication.data.model.DeviceEntity
import com.example.testapplication.data.remote.api.DeviceService
import com.example.testapplication.utils.Platform
import com.example.testapplication.utils.generateHomeId
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DeviceRepository @Inject constructor(
    private val deviceService: DeviceService,
    private val deviceDao: DeviceDao,
    private val context: Context

) {

    suspend fun fetchDevices() = withContext(Dispatchers.IO) {
        val devicesInDB = deviceDao.getAllDevices()

        if (devicesInDB.isEmpty()) {
            val deviceListResponse = deviceService.getDevices()
            Log.e("TAG_RESPONSE", deviceListResponse.toString())
            val deviceResponses = deviceListResponse.devices
            val deviceEntities = deviceResponses.map { response ->
                val platformEnum = Platform.fromString(response.platform)
                DeviceEntity(
                    pkDevice = response.pkDevice,
                    macAddress = response.macAddress,
                    firmware = response.firmware,
                    deviceModel = platformEnum.resourceName,
                    resourceId = platformEnum.resourceId,
                    homeTitle = generateHomeId().toString()
                )
            }

            deviceDao.insertDevices(deviceEntities)
        }
    }

    suspend fun getAllDevices(): List<DeviceEntity> = withContext(Dispatchers.IO) {
        deviceDao.getAllDevices()
    }

    suspend fun getDeviceById(pkDevice: Int): DeviceEntity? = withContext(Dispatchers.IO) {
        deviceDao.getDeviceById(pkDevice)
    }

    suspend fun updateDevice(device: DeviceEntity) = withContext(Dispatchers.IO) {
        deviceDao.updateDevice(device)
    }

    suspend fun deleteDevice(device: DeviceEntity) = withContext(Dispatchers.IO) {
        deviceDao.deleteDevice(device)
    }

    suspend fun deleteDeviceById(pkDevice: Int) = withContext(Dispatchers.IO) {
        deviceDao.deleteDeviceById(pkDevice)
    }

    suspend fun deleteAllDevices() = withContext(Dispatchers.IO) {
        deviceDao.deleteAllDevices()
    }
}