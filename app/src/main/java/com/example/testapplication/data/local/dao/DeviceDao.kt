package com.example.testapplication.data.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.example.testapplication.data.model.DeviceEntity

@Dao
interface DeviceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDevices(devices: List<DeviceEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDevice(device: DeviceEntity)

    @Query("SELECT * FROM device ORDER BY pkDevice")
    suspend fun getAllDevices(): List<DeviceEntity>

    @Query("SELECT * FROM device WHERE pkDevice = :pkDevice")
    suspend fun getDeviceById(pkDevice: Int): DeviceEntity

    @Update
    suspend fun updateDevice(device: DeviceEntity)

    @Delete
    suspend fun deleteDevice(device: DeviceEntity)

    @Query("DELETE FROM device WHERE pkDevice = :pkDevice")
    suspend fun deleteDeviceById(pkDevice: Int)

    @Query("DELETE FROM device")
    suspend fun deleteAllDevices()
}