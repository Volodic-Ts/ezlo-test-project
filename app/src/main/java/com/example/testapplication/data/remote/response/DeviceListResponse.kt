package com.example.testapplication.data.remote.response

import com.google.gson.annotations.SerializedName

data class DeviceListResponse(
    @SerializedName("Devices") val devices: List<DeviceResponse>
)
