package com.example.testapplication.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "device")
data class DeviceEntity(
    @PrimaryKey val pkDevice: Int,
    val macAddress: String,
    val deviceModel: String,
    val firmware: String,
    val homeTitle: String,
    val resourceId: Int
) : Parcelable